package com.owen.material3sample.listpage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.displayCutoutPadding
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.bumptech.glide.integration.compose.placeholder
import com.owen.material3sample.R
import com.owen.material3sample.databinding.FragmentListBinding

private val data = ArrayList<ListFragment.Item>()

class ListFragment : Fragment() {

    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentListBinding.inflate(inflater).also { _binding = it }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mackData()

        binding.composeView.setContent {
            Box {
                val myList = remember { mutableStateListOf<Item>() }
                myList.addAll(data)
                ListVerticalView(myList)
                FloatingActionButton(
                    modifier = Modifier
                        .align(Alignment.BottomEnd)
                        .padding(16.dp) // normal 16dp of padding for FABs
                        .navigationBarsPadding() // padding for navigation bar
                        .imePadding(), // padding for when IME appears
                    onClick = {
                        data.reverse()
                        myList.clear()
                        myList.addAll(data)
                    }
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.baseline_reply_24),
                        contentDescription = null
                    )
                }
            }
        }
    }

    private fun mackData() {
        for (i in 0 until 30) {
            val category = when (i) {
                in 10..19 -> "b"
                in 20..29 -> "c"
                else -> "a"
            }
            val url = if (i % 2 == 0) {
                "https://assets-global.website-files.com/60d196db74a1e36ff16ebd5d/63c117f19e248b737369b165_puppy-g3237d6104_1920.jpg"
            } else {
                ""
            }

            data.add(
                Item(
                    id = i.toString(),
                    url = url,
                    name = "item $i",
                    category = category
                )
            )
        }
    }

    @Composable
    private fun ListVerticalView(data: List<Item>) {
        val scrollState = rememberLazyListState()
        LazyColumn(
            verticalArrangement = Arrangement.spacedBy(4.dp),
            modifier = Modifier
                .displayCutoutPadding()
                .padding(horizontal = 12.dp)
                .fillMaxSize(),
            state = scrollState
        ) {
            items(
                items = data,
                key = { item ->
                    return@items item.id
                }
            ) { item ->
                ItemView(item)
            }
        }
    }

    @OptIn(ExperimentalGlideComposeApi::class)
    @Composable
    private fun ItemView(item: Item) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(vertical = 4.dp)
        ) {
            GlideImage(
                model = item.url,
                contentDescription = item.name,
                modifier = Modifier.size(48.dp),
                loading = placeholder(R.drawable.baseline_circle_32),
                contentScale = ContentScale.Crop,

                ) {
                it.circleCrop()
            }
            Text(
                text = item.name,
                style = MaterialTheme.typography.bodyLarge,
                color = MaterialTheme.colorScheme.onPrimary,
                modifier = Modifier
                    .padding(4.dp)
                    .fillMaxWidth()
            )
        }
    }

    data class Item(
        val id: String,
        val url: String,
        val name: String,
        val category: String
    )

    companion object {

        @JvmStatic
        fun newInstance() = ListFragment()
    }
}