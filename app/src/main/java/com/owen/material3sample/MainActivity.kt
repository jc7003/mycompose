package com.owen.material3sample

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.forEachIndexed
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.owen.material3sample.databinding.ActivityMainBinding
import com.owen.material3sample.home.HomeFragment
import com.owen.material3sample.listpage.ListFragment
import com.owen.material3sample.widgetpage.WidgetFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val menuMap = mutableMapOf<MenuItem, Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(binding) {
            with(navigationBar) {
                menu.forEachIndexed { index, item ->
                    menuMap[item] = index
                }
                setOnItemSelectedListener {
                    run {
                        val currentIndex = menuMap[it] ?: throw IllegalArgumentException("Illegal menu")
                        if (viewpager2.currentItem == currentIndex){
                            return@run
                        }

                        viewpager2.setCurrentItem(currentIndex, false)
                    }

                    return@setOnItemSelectedListener true
                }
            }

            with(viewpager2) {
                adapter = MainPagerAdapter()

                registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                    override fun onPageSelected(position: Int) {
                        navigationBar.selectedItemId = navigationBar.menu[position].itemId
                    }
                })
            }
        }

    }


    private inner class MainPagerAdapter : FragmentStateAdapter(this) {
        override fun getItemCount() = pages.size

        override fun createFragment(position: Int): Fragment {
            return when(position) {
                PAGE_HOME -> {
                    HomeFragment.newInstance("Home")
                }
                PAGE_WIDGET -> {
                    WidgetFragment.newInstance()
                }
                PAGE_LIST -> {
                    ListFragment.newInstance()
                }
                PAGE_DESIGN -> {
                    HomeFragment.newInstance("Design")
                }
                PAGE_USER -> {
                    HomeFragment.newInstance("User")
                }

                else -> {
                    throw IllegalAccessException("Illegal position")
                }
            }
        }
    }

    companion object {

        private const val PAGE_HOME = 0
        private const val PAGE_WIDGET = 1
        private const val PAGE_LIST = 2
        private const val PAGE_DESIGN = 3
        private const val PAGE_USER = 4

        private val pages = arrayListOf(
            PAGE_HOME,
            PAGE_WIDGET,
            PAGE_LIST,
            PAGE_DESIGN,
            PAGE_USER
        )

    }
}